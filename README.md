In den Einzelnen Fächern befinden sich .tex Dateien (LaTeX), PDFs (kompelierte .tex Datei) und evt. einen Ordner mit Bildern.

Gerne können die einzelnen Datein bearbeitet und verbessert werden. Eventuell muss eine neue Fork oder Branch angelegt werden.

Die .tex Dateien können bearbeitet werden. Dazu bieten sich Online-Latex Programme an. Zum Beispiel Overleaf im Browser. Auch Offline-Programme wie Kile könne dazu genutz werden. Ich nutze den Kompiler Xelatex.
