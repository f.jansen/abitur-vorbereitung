<h1>Geschichte LK</h1>

Bei diesem Text handelt es sich um eine Zusammenfassung der Lerninhalte der Sekundarstufe II meines Geschichts Leistungskurs. Ich habe hier die Inhalte so festgehalten wie ich sie für spätere Prüfungen relevant halte. Die Informationen sind zum größten Teil Aufzeichnungen zu den Aufgaben aus dem Unterricht sowie Mitschriften aus dem Unterricht. In kleinen Teilen wurden Informationen aus dem Internet zugezogen. (Angaben sind ohne Gewähr :-)

Viele Texte sind noch nicht optimal (Satzbau, Zeitform, Rechtschreibung, Inhalt) gerne können Änderungen vorgenommen werden. 
